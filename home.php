
<div class="row">
<div class="col-lg-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                    <div class="col-md-4 mt-2 mb-2">
                            <div class="card">
                                    <div class="seo-fact sbg1">
                                        <div class="p-4 d-flex justify-content-between align-items-center">
                                            <div class="seofct-icon"><i class="fa fa-briefcase"></i> Inventaris</div>
                                            <?php
                                            $cou=$konek->query("SELECT COUNT(id_inventaris) as jum_in FROM inventaris");
                                            while($da=$cou->fetch_array()){
                                                ?>
                                            <h2><?=$da['jum_in']?></h2>
                                        <?php
                                        }
                                            ?>
                                        </div>
                                        <canvas id="seolinechart1" height="30"></canvas>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 mt-2 mb-2">
                            <div class="card">
                                    <div class="seo-fact sbg1">
                                        <div class="p-4 d-flex justify-content-between align-items-center">
                                            <div class="seofct-icon" style="font-size: 13px"><i class="ti-layout-media-right"></i> Peminjaman Hari Ini</div>
                                            <?php
                                            $tgl=date("Y-m-d");
                                            $cou=$konek->query("SELECT SUM(jumlah_p) as jum_in FROM detail_pinjam d JOIN peminjaman p ON d.id_peminjaman=p.id_peminjaman WHERE tanggal_pinjam='$tgl' AND status_peminjaman='Pinjam'");
                                            $cou2=$konek->query("SELECT SUM(jumlah_p_p) as jum_in2 FROM detail_pinjam_p d JOIN peminjaman_p p ON d.id_peminjaman_p=p.id_peminjaman_p WHERE tanggal_pinjam='$tgl' AND status_peminjaman_pe='Pinjam'");
                                            $cou3=$konek->query("SELECT SUM(jumlah_p_s) as jum_in3 FROM detail_pinjam_s d JOIN peminjaman_s p ON d.id_peminjaman_s=p.id_peminjaman_s WHERE tanggal_pinjam='$tgl' AND status_peminjaman_s='Pinjam'");
                                            while($da=$cou->fetch_array()){
                                                while($da2=$cou2->fetch_array()){
                                                    while($da3=$cou3->fetch_array()){
                                                ?>
                                            <h2><?=$da['jum_in']+$da2['jum_in2']+$da3['jum_in3']?></h2>
                                        <?php
                                    }
                                    }
                                        }
                                            ?>
                                        </div>
                                        <canvas id="seolinechart1" height="30"></canvas>
                                    </div>
                                </div>
                            </div>

                        <div class="col-md-4 mt-2 mb-2">
                            <div class="card">
                                    <div class="seo-fact sbg1">
                                        <div class="p-4 d-flex justify-content-between align-items-center">
                                            <div class="seofct-icon" style="font-size: 13px"><i class="ti-layout-media-left"></i> Pengembalian Hari Ini</div>
                                            <?php
                                            $tgl=date("Y-m-d");
                                            $cou=$konek->query("SELECT SUM(jumlah_p) as jum_in FROM detail_pinjam d JOIN peminjaman p ON d.id_peminjaman=p.id_peminjaman WHERE tanggal_pinjam='$tgl' AND status_peminjaman='Kembali'");
                                            $cou2=$konek->query("SELECT SUM(jumlah_p_p) as jum_in2 FROM detail_pinjam_p d JOIN peminjaman_p p ON d.id_peminjaman_p=p.id_peminjaman_p WHERE tanggal_pinjam='$tgl' AND status_peminjaman_pe='Kembali'");
                                            $cou3=$konek->query("SELECT SUM(jumlah_p_s) as jum_in3 FROM detail_pinjam_s d JOIN peminjaman_s p ON d.id_peminjaman_s=p.id_peminjaman_s WHERE tanggal_pinjam='$tgl' AND status_peminjaman_s='Kembali'");
                                            while($da=$cou->fetch_array()){
                                                while($da2=$cou2->fetch_array()){
                                                    while($da3=$cou3->fetch_array()){
                                                ?>
                                            <h2><?=$da['jum_in']+$da2['jum_in2']+$da3['jum_in3']?></h2>
                                        <?php
                                    }
                                    }
                                        }
                                            ?>
                                        </div>
                                        <canvas id="seolinechart1" height="30"></canvas>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <br>
<div class="row">
    <div class="col-md-12">
    <div style="width: 80%;margin: 0px auto;">
        <canvas id="myChart2"></canvas>
    </div>
            <script>
            <?php 
            $sql1 = $konek->query("SELECT SUM(jumlah_p) as total_p FROM detail_pinjam d JOIN peminjaman p ON d.id_peminjaman=p.id_peminjaman WHERE p.status_peminjaman='Pinjam'");
            $sql2 = $konek->query("SELECT SUM(jumlah_p_p) as total_p_p FROM detail_pinjam_p d JOIN peminjaman_p p ON d.id_peminjaman_p=p.id_peminjaman_p WHERE p.status_peminjaman_pe='Pinjam'");
            $sql3 = $konek->query("SELECT SUM(jumlah_p_s) as total_p_s FROM detail_pinjam_s d JOIN peminjaman_s p ON d.id_peminjaman_s=p.id_peminjaman_s WHERE p.status_peminjaman_s='Pinjam'");

             ?>
            var ctx = document.getElementById("myChart2");
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels:['Peminjaman Petugas','Peminjaman Pegawai','Peminajaman Siswa'],
                    datasets: [{
                            label:'Daftar List Jumlah Peminjaman',
                            data: [
                            <?php while ($jml = $sql1->fetch_array()) { echo '"' . $jml[total_p] . '",';}?>
                            <?php while ($jml2 = $sql2->fetch_array()) { echo '"' . $jml2[total_p_p] . '",';}?>
                            <?php while ($jml3 = $sql3->fetch_array()) { echo '"' . $jml3s[total_p_s] . '",';}?>
                            ],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)'
                            ],
                            borderWidth: 1
                        }]
                },
                options: {
                    scales: {
                        yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                    }
                }
            });
</script>
</div>

</div>
<br>
<div class="row">
    <div style="width: 80%;margin: 0px auto;">
        <canvas id="myChart3"></canvas>
    </div>
            <script>
            <?php 
            $sql1 = $konek->query("SELECT SUM(jumlah_p) as total_p FROM detail_pinjam d JOIN peminjaman p ON d.id_peminjaman=p.id_peminjaman WHERE p.status_peminjaman='Kembali'");
            $sql2 = $konek->query("SELECT SUM(jumlah_p_p) as total_p_p FROM detail_pinjam_p d JOIN peminjaman_p p ON d.id_peminjaman_p=p.id_peminjaman_p WHERE p.status_peminjaman_pe='Kembali'");
            $sql3 = $konek->query("SELECT SUM(jumlah_p_s) as total_p_s FROM detail_pinjam_s d JOIN peminjaman_s p ON d.id_peminjaman_s=p.id_peminjaman_s WHERE p.status_peminjaman_s='Kembali'");
             ?>
            var ctx = document.getElementById("myChart3");
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels:['Pengembalian Petugas','Pengembalian Pegawai','Pengembalian Petugas'],
                    datasets: [{
                            label:'Daftar List Jumlah Pengembalian',
                            data: [
                            <?php while ($jml = $sql1->fetch_array()) { echo '"' . $jml[total_p] . '",';}?>
                            <?php while ($jml2 = $sql2->fetch_array()) { echo '"' . $jml2[total_p_p] . '",';}?>
                            <?php while ($jml3 = $sql3->fetch_array()) { echo '"' . $jml3s[total_p_s] . '",';}?>
                            ],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)'
                            ],
                            borderWidth: 1
                        }]
                },
                options: {
                    scales: {
                        yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                    }
                }
            });
</script>
</div>
                </div>
            </div>
        </div>
    </div>