<?php 
	session_start();
    $konek = new mysqli("localhost","root","","uji_kom");
	
	if ($_SESSION['Administrator'] || $_SESSION['Petugas']) {
    	header("location:index.php");
    }else{


 ?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="assets/images/icon/favicon.ico">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/metisMenu.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/slicknav.min.css">
    <!-- amchart css -->
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <!-- others css -->
    <link rel="stylesheet" href="assets/css/typography.css">
    <link rel="stylesheet" href="assets/css/default-css.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <!-- modernizr css -->
    <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!-- login area start -->
    <div class="login-area">
        <div class="container">
            <div class="login-box ptb--100">
                <form action="" method="POST">
                    <div class="login-form-head">
                        <h4>Masuk</h4>                        
                    </div>
                    <div class="login-form-body">
                        <div class="form-gp">
                            <label for="exampleInputEmail1">Username</label>
                            <input type="text" name="username" id="exampleInputEmail1" pattern="[a-z A-Z 0-9]+" required="">
                            <i class="ti-user"></i>
                        </div>
                        <div class="form-gp">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="email" name="email" id="exampleInputPassword1" pattern="[a-z A-Z 0-9 @ .]+" required="">
                            <i class="ti-email"></i>
                        </div>
                        <div class="row mb-4 rmber-area">
                            <div class="col-6">                                
                            </div>
                            <div class="col-6 text-right">
                                
                            </div>
                        </div>
                        <div class="submit-btn-area">
                            <button id="form_submit" type="submit" name="masuk">Masuk <i class="ti-arrow-right"></i></button>
                            <div class="login-other row mt-4">
                                <div class="col-6">
                                    <a class="" href="login2.php">Login Sebagai Pegawai</a>
                                </div>
                                <div class="col-6">
                                    <a class="" href="login3.php">Login Sebagai Siswa </a>
                                </div>
                            </div>
                            <div class="form-footer text-center mt-5">
                            <a href="panduan/help.php">Help</a></p>
                        </div>
                        </div>
                    </div>
<?php 

if (isset($_POST['submit'])) {
	include 'koneksi.php';
	$username = $_POST['username'];
	$email = $_POST['email'];

	$query = $konek->query("SELECT * FROM petugas WHERE username='$username' AND email='$email'  ");
	$hitung = $query->num_rows;
	$data = $query->fetch_assoc();

	if ($hitung==1) {
	    $subject="LUPA PASSWORD";
	    $text="Password Anda Untuk username : $username dan email : $email adalah $data[password]";
	    $head= "From: Inventaris SMK<no-reply@domain.com";
	    
		$kirim = mail("$email", "$subject", "$text", "$head");
		if ($kirim) {
			?>
			<script type="text/javascript">
                      alert("Password telah dikirim ke alamat email");
             </script>
			<?php
		}
	}else{
		?>
		<script type="text/javascript">
            alert("Email tidak ditemukan");
        </script>
<?php
	}
}
?>

                </form>
            </div>
        </div>
    </div>
    <!-- login area end -->

    <!-- jquery latest version -->
    <script src="assets/js/vendor/jquery-2.2.4.min.js"></script>
    <!-- bootstrap 4 js -->
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/metisMenu.min.js"></script>
    <script src="assets/js/jquery.slimscroll.min.js"></script>
    <script src="assets/js/jquery.slicknav.min.js"></script>
    
    <!-- others plugins -->
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/scripts.js"></script>
</body>

</html>
<?php 
} ?>