-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 11 Apr 2019 pada 06.54
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uji_kom`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pinjam`
--

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL,
  `id_inventaris_d` int(11) NOT NULL,
  `kode_peminjaman` varchar(50) NOT NULL,
  `jumlah_p` int(11) NOT NULL,
  `rusak_d` int(11) NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_pinjam`
--

INSERT INTO `detail_pinjam` (`id_detail_pinjam`, `id_inventaris_d`, `kode_peminjaman`, `jumlah_p`, `rusak_d`, `id_peminjaman`, `id_petugas`) VALUES
(53, 1, 'PJM-8424955343', 1, 0, 57, 5),
(54, 3, 'PJM-8424955343', 1, 0, 58, 5),
(55, 1, 'PJM-7442790654', 2, 0, 59, 2),
(56, 3, 'PJM-7442790654', 1, 0, 60, 2),
(57, 3, 'PJM-4415293420', 1, 0, 61, 5),
(59, 3, 'PJM-2161554675', 1, 0, 63, 5),
(61, 3, 'PJM-1658650832', 3, 1, 70, 5),
(63, 3, 'PJM-6412617671', 3, 1, 72, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pinjam_p`
--

CREATE TABLE `detail_pinjam_p` (
  `id_detail_pinjam_p` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `kode_peminjaman_d_p` varchar(50) NOT NULL,
  `jumlah_p_p` int(11) NOT NULL,
  `rusak_d_p` int(11) NOT NULL,
  `id_peminjaman_p` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_pinjam_p`
--

INSERT INTO `detail_pinjam_p` (`id_detail_pinjam_p`, `id_inventaris`, `kode_peminjaman_d_p`, `jumlah_p_p`, `rusak_d_p`, `id_peminjaman_p`, `id_pegawai`) VALUES
(13, 1, 'PJM-7866345324', 1, 0, 14, 8),
(14, 3, 'PJM-7866345324', 1, 0, 15, 8),
(17, 3, 'PJM-6912096431', 2, 1, 18, 8);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pinjam_s`
--

CREATE TABLE `detail_pinjam_s` (
  `id_detail_pinjam_s` int(11) NOT NULL,
  `id_inventaris_d_s` int(11) NOT NULL,
  `kode_peminjaman_d_s` varchar(50) NOT NULL,
  `jumlah_p_s` int(11) NOT NULL,
  `rusak_d_s` int(11) NOT NULL,
  `id_peminjaman_s` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_pinjam_s`
--

INSERT INTO `detail_pinjam_s` (`id_detail_pinjam_s`, `id_inventaris_d_s`, `kode_peminjaman_d_s`, `jumlah_p_s`, `rusak_d_s`, `id_peminjaman_s`, `id_siswa`) VALUES
(7, 1, 'PJM-0454734822', 1, 0, 7, 1),
(8, 8, 'PJM-0864561555', 1, 0, 8, 1),
(10, 3, 'PJM-0253306653', 3, 1, 10, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `inventaris`
--

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `kondisi` varchar(25) NOT NULL,
  `keterangan` tinytext NOT NULL,
  `jumlah` int(11) NOT NULL,
  `rusak_inven` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` varchar(25) NOT NULL,
  `id_sarana` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `inventaris`
--

INSERT INTO `inventaris` (`id_inventaris`, `nama`, `kondisi`, `keterangan`, `jumlah`, `rusak_inven`, `id_jenis`, `tanggal_register`, `id_ruang`, `kode_inventaris`, `id_sarana`, `id_petugas`, `foto`) VALUES
(1, 'Laptop', 'Baik', '-', 72, 42, 1, '2019-01-09', 1, '675764', 1, 2, 'barang.png'),
(3, 'trtfg', 'Baik', '-', 45, 4, 1, '2019-01-25', 1, 'sadasf', 1, 5, 'barang.png'),
(9, 'Infokus', 'Baik', 'INI INFOKUS', 0, 0, 1, '2019-03-07', 1, '2412312312', 1, 5, 'barang.png'),
(11, 'obeng', 'Baik', '-', 5, 0, 2, '2019-04-09', 2, '1231223', 2, 5, 'a.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(25) NOT NULL,
  `kode_jenis` varchar(25) NOT NULL,
  `keterangan_j` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan_j`) VALUES
(1, 'Elektronik', '78658', 'Elektronik'),
(2, 'KBM', '68566', '-'),
(4, 'Olahraga', '68567', 'BOLA KU Yang Keren'),
(9, 'IMPACTER', '18638', 'TOOLS'),
(11, 'Agent', '007', 'Alatnya James Bond Tapi Boong hehehe');

-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL,
  `nama_level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'Administrator'),
(2, 'Petugas');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nama_pegawai` varchar(25) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `nip`, `alamat`) VALUES
(8, 'Doni', '870238412341232', 'INDONESIA SELATAN BAGIAN UJUNGIH'),
(10, 'Rendi', '870238412341234', 'INDONESIA SELATAN BAGIAN UJUNGIH');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL,
  `kode_peminjaman_p` varchar(50) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_peminjaman` varchar(25) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peminjaman`
--

INSERT INTO `peminjaman` (`id_peminjaman`, `kode_peminjaman_p`, `tanggal_pinjam`, `tanggal_kembali`, `status_peminjaman`, `id_inventaris`, `id_petugas`) VALUES
(57, 'PJM-8424955343', '2019-04-06', '0000-00-00', 'Pinjam', 1, 5),
(58, 'PJM-8424955343', '2019-04-06', '0000-00-00', 'Pinjam', 3, 5),
(59, 'PJM-7442790654', '2019-04-06', '2019-04-09', 'Kembali', 1, 2),
(60, 'PJM-7442790654', '2019-04-06', '2019-04-06', 'Kembali', 3, 2),
(61, 'PJM-4415293420', '2019-04-06', '0000-00-00', 'Pinjam', 3, 5),
(62, 'PJM-2161554675', '2019-04-07', '0000-00-00', 'Pinjam', 8, 5),
(63, 'PJM-2161554675', '2019-04-07', '0000-00-00', 'Pinjam', 3, 5),
(64, 'PJM-9109247779', '2019-04-07', '2019-04-09', 'Kembali', 8, 2),
(65, 'PJM-6740681570', '2019-04-07', '0000-00-00', 'Pinjam', 8, 13),
(66, 'PJM-0137240191', '2019-04-09', '2019-04-09', 'Kembali', 8, 5),
(67, 'PJM-2615678408', '2019-04-09', '2019-04-09', 'Kembali', 8, 5),
(68, 'PJM-4579405043', '2019-04-09', '2019-04-09', 'Kembali', 8, 5),
(70, 'PJM-1658650832', '2019-04-09', '2019-04-09', 'Kembali', 3, 5),
(72, 'PJM-6412617671', '2019-04-09', '2019-04-09', 'Kembali', 3, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman_p`
--

CREATE TABLE `peminjaman_p` (
  `id_peminjaman_p` int(11) NOT NULL,
  `kode_peminjaman_pe` varchar(50) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_peminjaman_pe` varchar(50) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peminjaman_p`
--

INSERT INTO `peminjaman_p` (`id_peminjaman_p`, `kode_peminjaman_pe`, `tanggal_pinjam`, `tanggal_kembali`, `status_peminjaman_pe`, `id_inventaris`, `id_pegawai`) VALUES
(14, 'PJM-7866345324', '2019-04-06', '0000-00-00', 'Pinjam', 1, 8),
(15, 'PJM-7866345324', '2019-04-06', '0000-00-00', 'Pinjam', 3, 8),
(18, 'PJM-6912096431', '2019-04-09', '2019-04-09', 'Kembali', 3, 8);

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman_s`
--

CREATE TABLE `peminjaman_s` (
  `id_peminjaman_s` int(11) NOT NULL,
  `kode_peminjaman_s` varchar(50) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_peminjaman_s` varchar(25) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peminjaman_s`
--

INSERT INTO `peminjaman_s` (`id_peminjaman_s`, `kode_peminjaman_s`, `tanggal_pinjam`, `tanggal_kembali`, `status_peminjaman_s`, `id_inventaris`, `id_siswa`) VALUES
(7, 'PJM-0454734822', '2019-04-06', '0000-00-00', 'Pinjam', 1, 1),
(8, 'PJM-0864561555', '2019-04-09', '0000-00-00', 'Pinjam', 8, 1),
(10, 'PJM-0253306653', '2019-04-09', '2019-04-09', 'Kembali', 3, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_petugas` varchar(25) NOT NULL,
  `email` varchar(50) NOT NULL,
  `user_key` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `id_level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `username`, `password`, `nama_petugas`, `email`, `user_key`, `status`, `id_level`) VALUES
(2, 'reza', '526e34d04735124f05a090181f3e6e8a', 'Reza Setiawan', 'kingkabuto7@gmail.com', '', 0, 2),
(5, 'raja', '098f6bcd4621d373cade4e832627b4f6', 'Mohamad Raja Fakhri', 'mrifkyafrizal@gmail.com', '', 1, 1),
(10, 'tele', '526e34d04735124f05a090181f3e6e8a', 'tele', 'sd', '', 0, 1),
(11, 'kevin', '9d5e3ecdeb4cdb7acfd63075ae046672', 'Kevin', 'sdd', '', 0, 1),
(12, 'asdasdddd', '3dad9cbf9baaa0360c0f2ba372d25716', 'asdddd', 'kingasgdjasd@asjdghasd', '', 0, 1),
(13, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'admin_test@gmail.com', '', 0, 1),
(14, 'operator', '4b583376b2767b923c3e1da60d10de59', 'operator', 'operator_test@gmail.com', '', 0, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruang`
--

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL,
  `nama_ruang` varchar(25) NOT NULL,
  `kode_ruang` varchar(25) NOT NULL,
  `keterangan_r` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan_r`) VALUES
(1, 'Lab RPL 1', '231454', 'Testing'),
(2, 'asdsa1234', '12341', 'asfasdASFASD'),
(11, 'Lab Animasi 1', '33311', 'asfasdasfsad');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sarana`
--

CREATE TABLE `sarana` (
  `id_sarana` int(11) NOT NULL,
  `nama_sarana` varchar(50) NOT NULL,
  `kode_sarana` varchar(50) NOT NULL,
  `foto_s` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sarana`
--

INSERT INTO `sarana` (`id_sarana`, `nama_sarana`, `kode_sarana`, `foto_s`) VALUES
(1, 'Rekayasa Perangkat Lunak', '12324123', 'rpl.jpg'),
(2, 'ANIMASI', '12312', 'anm.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE `siswa` (
  `id_siswa` int(11) NOT NULL,
  `nama_siswa` varchar(50) NOT NULL,
  `nis` char(15) NOT NULL,
  `alamat` text NOT NULL,
  `jk` varchar(20) NOT NULL,
  `no_hp` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`id_siswa`, `nama_siswa`, `nis`, `alamat`, `jk`, `no_hp`) VALUES
(1, 'Fathurohman Nurasyam', '123412512342123', 'Pintu Ledeng Ke Dalam', 'Laki - Laki', '089779797989');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD PRIMARY KEY (`id_detail_pinjam`),
  ADD KEY `detail_pinjam_ibfk_1` (`id_inventaris_d`);

--
-- Indexes for table `detail_pinjam_p`
--
ALTER TABLE `detail_pinjam_p`
  ADD PRIMARY KEY (`id_detail_pinjam_p`);

--
-- Indexes for table `detail_pinjam_s`
--
ALTER TABLE `detail_pinjam_s`
  ADD PRIMARY KEY (`id_detail_pinjam_s`);

--
-- Indexes for table `inventaris`
--
ALTER TABLE `inventaris`
  ADD PRIMARY KEY (`id_inventaris`),
  ADD KEY `id_jenis` (`id_jenis`),
  ADD KEY `id_ruang` (`id_ruang`),
  ADD KEY `id_petugas` (`id_petugas`),
  ADD KEY `inventaris_ibfk_4` (`id_sarana`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`),
  ADD KEY `peminjaman_ibfk_1` (`id_petugas`);

--
-- Indexes for table `peminjaman_p`
--
ALTER TABLE `peminjaman_p`
  ADD PRIMARY KEY (`id_peminjaman_p`);

--
-- Indexes for table `peminjaman_s`
--
ALTER TABLE `peminjaman_s`
  ADD PRIMARY KEY (`id_peminjaman_s`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`),
  ADD KEY `petugas_ibfk_1` (`id_level`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- Indexes for table `sarana`
--
ALTER TABLE `sarana`
  ADD PRIMARY KEY (`id_sarana`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  MODIFY `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `detail_pinjam_p`
--
ALTER TABLE `detail_pinjam_p`
  MODIFY `id_detail_pinjam_p` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `detail_pinjam_s`
--
ALTER TABLE `detail_pinjam_s`
  MODIFY `id_detail_pinjam_s` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `inventaris`
--
ALTER TABLE `inventaris`
  MODIFY `id_inventaris` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT for table `peminjaman_p`
--
ALTER TABLE `peminjaman_p`
  MODIFY `id_peminjaman_p` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `peminjaman_s`
--
ALTER TABLE `peminjaman_s`
  MODIFY `id_peminjaman_s` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `ruang`
--
ALTER TABLE `ruang`
  MODIFY `id_ruang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `sarana`
--
ALTER TABLE `sarana`
  MODIFY `id_sarana` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD CONSTRAINT `detail_pinjam_ibfk_3` FOREIGN KEY (`id_inventaris_d`) REFERENCES `inventaris` (`id_inventaris`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `inventaris`
--
ALTER TABLE `inventaris`
  ADD CONSTRAINT `inventaris_ibfk_2` FOREIGN KEY (`id_ruang`) REFERENCES `ruang` (`id_ruang`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `inventaris_ibfk_3` FOREIGN KEY (`id_jenis`) REFERENCES `jenis` (`id_jenis`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `petugas`
--
ALTER TABLE `petugas`
  ADD CONSTRAINT `petugas_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
