
<div class="card-area">
        <div class="row">            
            <?php foreach ($db->sarana() as $d) {
            ?>
            <div class="col-lg-4 col-md-6 mt-5">
                <div class="card card-bordered">
                    <img class="card-img-top img-fluid" src="assets/images/inventaris/<?php echo $d['foto_s']?>" style="height: 200px;" alt="image">
                    <div class="card-body">
                        <h5 class="title"><?php echo $d['nama_sarana'] ?></h5>
                        <p class="card-text">
                        </p>
                        <a href="?page=peminjaman_pegawai&opsi=peminjaman_detail_op&id_sarana=<?php echo $d['id_sarana']; ?>&kode_pjm=<?php echo $kode ?>" class="btn btn-primary">Go More....</a>
                    </div>
                </div>
            </div>
            <?php
            }
            ?>
</div>
</div>