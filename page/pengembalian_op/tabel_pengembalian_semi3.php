<?php
   $id_siswa = $_GET['id_siswa'];
   $nama_siswa = $_GET['nama_siswa'];
   $id_sarana=$_GET['id_sarana'];
?>
<div class="row">
<div class="col-lg-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Data Peminjaman Siswa</h4>
                    <div class="single-table">
                        <div class="table-responsive">
                            <div class="row">
                            <div class="col-lg-2">
                            <span><input type="text" class="form-control" style="width: 10rem;" value="<?php echo $nama_siswa; ?>" readonly></span>
                            </div>
                            </div>                            
                            <br>
                            <table class="table text-center" id="example">
                                <thead class="text-uppercase bg-primary">
                                    <tr class="text-white">
                                        <th scope="col">NO</th>
                                        <th scope="col">Kode Peminjaman</th>
                                        <th scope="col">Jumlah Yang Di Pinjam</th>
                                        <th scope="col">Tanggal Pinjam</th>                                  
                                        <th scope="col">Status</th>                                  
                                        <th scope="col">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
                                $no = 1;
                                foreach($db->pengembalian_table_siswa($id_siswa,$id_sarana) as $tb){
                                    
                                ?>
                                <tr>
                                    <th scope="row"><?php echo $no++; ?></th>
                                    <td><?=$tb['kode_peminjaman_s']?></td>
                                    <td><?php 
                                    $k=$tb['kode_peminjaman_s'];
                                         $sum=$konek->query("SELECT SUM(jumlah_p_s) as total FROM detail_pinjam_s WHERE kode_peminjaman_d_s='$k'");
                                          while($da=$sum->fetch_array()){
                                           echo $da['total'];
                                          }
                                    ?></td>
                                    <td><?php echo date('d F Y', strtotime($tb['tanggal_pinjam'])); ?></td>
                                    <td><?php 
                                    $k=$tb['kode_peminjaman_s'];
                                         $sql=$konek->query("SELECT status_peminjaman_s FROM peminjaman_s WHERE kode_peminjaman_s='$k' AND status_peminjaman_s='Pinjam'");
                                         $cek = $sql->num_rows;
                                         $data = $sql->fetch_array();
                                         if ($cek >= 1) {
                                             echo"Pinjam";
                                         }else{
                                            echo "Kembali";
                                         }
                                          
                                    ?></td>                                    
                                    <td>
                                        <a href="?page=pengembalian_operator&opsi=tabel_view_op3&kode_peminjaman=<?php echo $tb['kode_peminjaman_s']; ?>&nama_siswa=<?php echo $tb['nama_siswa']; ?>"><i class="btn btn-primary ti-eye"></i></a>    
                                    </td>
                                </tr>
                            <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                </div>
                    </div>
                </div>
        </div>
        </div>
</div>