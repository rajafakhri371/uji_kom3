<div class="row">
<div class="col-lg-12 mt-5">
            <div class="card">
                <div class="card-body">
                        <div class="card">
                            <div class="card-body">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="petugas-tab" data-toggle="tab" href="#petugas" role="tab" aria-controls="petugas" aria-selected="true">Petugas</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pegawai-tab" data-toggle="tab" href="#pegawai" role="tab" aria-controls="pegawai" aria-selected="false">Pegawai</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Siswa</a>
                                    </li>
                                </ul>
                    <div class="tab-content mt-3" id="myTabContent">
                    <div class="tab-pane fade show active" id="petugas" role="tabpanel" aria-labelledby="home-tab">
                    <p><h4 class="header-title">Data Peminjaman Petugas</h4>
                    <div class="single-table">
                        <div class="table-responsive">
                            <table class="table text-center" id="example">
                                <thead class="text-uppercase bg-primary">
                                    <tr class="text-white">
                                        <th scope="col">NO</th>
                                        <th scope="col">Nama Peminjam</th>                                                          
                                        <th scope="col">Jumlah Total Pinjam</th>                                                       
                                        <th scope="col">Jumlah Pinjam Rusak</th>                                                       
                                        <th scope="col">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
                                $no = 1;
                                foreach($db->pengembalian_table2($_GET['id_sarana']) as $tb){                                   

                                ?>
                                <tr>
                                    <th scope="row"><?php echo $no++; ?></th>
                                    <td><?php echo $tb['nama_petugas']; ?></td>  
                                    <td>
                                        <?php
                                        $id=$tb['id_petugas'];
                                        $sum=$konek->query("SELECT SUM(jumlah_p) as total FROM detail_pinjam WHERE id_petugas='$id'");
                                          while($da=$sum->fetch_array()){
                                           echo $da['total'];
                                          }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $id=$tb['id_petugas'];
                                        $sum=$konek->query("SELECT SUM(rusak_d) as total2 FROM detail_pinjam WHERE id_petugas='$id'");
                                          while($da=$sum->fetch_array()){
                                           echo $da['total2'];
                                          }
                                        ?>
                                    </td>
                                    <td>
                                        <a href="?page=pengembalian_operator&opsi=tabel_pengembalian_op_semi&id_petugas=<?php echo $tb['id_petugas']; ?>&nama_petugas=<?php echo $tb['nama_petugas']; ?>&id_sarana=<?=$_GET['id_sarana']?>"><i class="btn btn-primary ti-eye"></i></a>                                        
                                    </td>
                                </tr>
                            <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div></p>
                    </div>
                    <div class="tab-pane fade" id="pegawai" role="tabpanel" aria-labelledby="pegawai-tab">
                    <p><h4 class="header-title">Data Peminjaman Pegawai</h4>
                        <div class="single-table">
                        <div class="table-responsive">
                            <table class="table text-center" id="example2">
                                <thead class="text-uppercase bg-primary">
                                    <tr class="text-white">
                                        <th scope="col">NO</th>
                                        <th scope="col">Nama Peminjam</th>
                                        <th scope="col">Jumlah Pinjam Seluruhnya</th>
                                        <th scope="col">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
                                $no = 1;
                                foreach($db->pengembalian_table3($_GET['id_sarana']) as $tb){

                                ?>
                                <tr>
                                    <th scope="row"><?php echo $no++; ?></th>
                                    <td><?php echo $tb['nama_pegawai']; ?></td>
                                    <td><?php
                                        $id_p=$tb['id_pegawai'];
                                        $sum=$konek->query("SELECT SUM(jumlah_p_p) as total FROM detail_pinjam_p WHERE id_pegawai='$id_p'");
                                          while($da=$sum->fetch_array()){
                                           echo $da['total'];
                                          }
                                        ?>
                                        </td>                                                        
                                    <td>
                                        <a href="?page=pengembalian_operator&opsi=tabel_pengembalian_op_semi2&id_pegawai=<?php echo $tb['id_pegawai']; ?>&nama_pegawai=<?php echo $tb['nama_pegawai']; ?>&id_sarana=<?=$_GET['id_sarana']?>"><i class="btn btn-primary ti-eye"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div></p>
                    </div>
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                    <p><h4 class="header-title">Data Peminjaman Siswa</h4>
                        <div class="single-table">
                        <div class="table-responsive">
                            <table class="table text-center" id="example2">
                                <thead class="text-uppercase bg-primary">
                                    <tr class="text-white">
                                        <th scope="col">NO</th>
                                        <th scope="col">Nama Peminjam</th>
                                        <th scope="col">Jumlah Total Pinjam</th>
                                        <th scope="col">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
                                $no = 1;
                                foreach($db->pengembalian_table4($_GET['id_sarana']) as $tb){

                                ?>
                                <tr>
                                    <th scope="row"><?php echo $no++; ?></th>
                                    <td><?php echo $tb['nama_siswa']; ?></td>
                                    <td><?php
                                        $id_s=$tb['id_siswa'];
                                        $sum=$konek->query("SELECT SUM(jumlah_p_s) as total FROM detail_pinjam_s WHERE id_siswa='$id_s'");
                                          while($da=$sum->fetch_array()){
                                           echo $da['total'];
                                          }
                                        ?>
                                        </td>                                                        
                                    <td>
                                        <a href="?page=pengembalian_operator&opsi=tabel_pengembalian_op_semi3&id_siswa=<?php echo $tb['id_siswa']; ?>&nama_siswa=<?php echo $tb['nama_siswa']; ?>&id_sarana=<?=$_GET['id_sarana']?>"><i class="btn btn-primary ti-eye"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>