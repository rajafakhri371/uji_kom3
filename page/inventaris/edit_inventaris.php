<div class="row">
<?php foreach ($db->edit_inventaris($_GET['id_inventaris']) as $d) {
    
 ?>
<div class="col-lg-6 col-ml-12">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h3>Ubah Data Inventaris</h3>
                    <p class="text-muted font-14 mb-4">Pastikan Untuk Mengisi Data Dengan Benar</p>
                    <form action="function/proses.php?aksi=update_inventaris1" method="POST" enctype="multipart/form-data">

                        <input class="form-control" type="hidden" name="id_petugas" id="example-text-input" required="" value="<?php echo $_SESSION['Administrator'] ?> ">
                        <input class="form-control" type="hidden" name="id_inventaris" id="example-text-input" required="" value="<?php echo $d['id_inventaris'] ?> ">

                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Nama Inventaris</label>
                        <input class="form-control" type="text" name="nama_inventaris" maxlength="25" id="example-text-input" required="" value="<?php echo $d['nama'] ?>" pattern="[a-z A-Z]+" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Kondisi</label>
                        <select class="form-control" name="kondisi" style="height: calc(3rem + 2px);">
                            <?php if ($d['kondisi']=='Baik') {
                            ?> 
                            <option value="Baik" selected>Baik</option>
                            <option value="Tidak Baik">Tidak Baik</option>

                        <?php
                        }else{
                        ?>
                            <option value="Baik">Baik</option>
                            <option value="Tidak Baik" selected>Tidak Baik</option>
                        <?php
                        }
                        ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">jumlah</label>
                        <input class="form-control" type="number" name="jumlah" maxlength="5" id="example-text-input" required="" value="<?php echo $d['jumlah'] ?>">
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Keterangan</label>
                        <textarea name="keterangan" class="form-control" id="" cols="30" rows="10" pattern="[a-z A-Z 0-9]+"><?php echo $d['keterangan'] ?></textarea>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Jenis</label>
                        <select class="form-control" name="jenis" style="height: calc(3rem + 2px);">
                            <?php 
                                foreach($db->in_jenis() as $ijs){
                                    if ($ijs['id_jenis']==$d['id_jenis']) {
                                       $selected="selected";
                                    }else{
                                        $selected="";
                                    }
                            ?>
                            <option value="<?php echo $ijs['id_jenis'] ?>"><?php echo $ijs['nama_jenis'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Ruangan</label>
                        <select class="form-control" name="ruang" style="height: calc(3rem + 2px);">
                            <?php 
                                foreach($db->in_ruang() as $ir){
                                    if ($ir['id_ruang']==$d['id_ruang']) {
                                       $selected="selected";
                                    }else{
                                        $selected="";
                                    }
                            ?>
                            <option value="<?php echo $ir['id_ruang'] ?>"><?php echo $ir['nama_ruang'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Sarana</label>
                        <select class="form-control" name="sarana" style="height: calc(3rem + 2px);">
                            <?php 
                                foreach($db->sarana() as $j){
                                    if ($j['id_sarana']==$d['id_sarana']) {
                                       $selected="selected";
                                    }else{
                                        $selected="";
                                    }
                            ?>
                            <option value="<?php echo $j['id_sarana'] ?>"><?php echo $j['nama_sarana'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Kode Inventaris</label>
                        <input class="form-control" type="text" name="kode_inventaris" maxlength="10" id="example-text-input" required="" value="<?php echo $d['kode_inventaris'] ?>" pattern="[a-z A-Z 0-9]+" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <img src="assets/images/inventaris/<?php echo $d['foto'] ?>" style="width:300px" alt="">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Upload</span>
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="inputGroupFile01" name="foto" required="" >
                            <label class="custom-file-label" for="inputGroupFile01" >Choose File</label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4"> Simpan</button>
                    </form>
                </div>
            </div>
        </div>
        <?php } ?>
</div>