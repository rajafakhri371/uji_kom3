<div class="row">

<div class="col-lg-6 col-ml-12">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h3>Tambah Data Inventaris</h3>
                    <p class="text-muted font-14 mb-4">Pastikan Untuk Mengisi Data Dengan Benar</p>
                    <form action="function/proses.php?aksi=tambah_inventaris" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Nama Inventaris</label>
                        <input class="form-control" type="hidden" name="id_petugas" maxlength="25" id="example-text-input" required="" value="<?php echo $_SESSION['Administrator'] ?> ">
                        <input class="form-control" type="text" name="nama_inventaris" maxlength="25" id="example-text-input" required="" pattern="[a-z A-Z]+" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Kondisi</label>
                        <select class="form-control" name="kondisi" style="height: calc(3rem + 2px);">
                            <option value="Baik">Baik</option>
                            <option value="Tidak Baik">Tidak Baik</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">jumlah</label>
                        <input class="form-control" type="number" name="jumlah" maxlength="5" id="example-text-input" required="">
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Keterangan</label>
                        <textarea name="keterangan" class="form-control" id="" cols="30" rows="10" pattern="[a-z A-Z 0-9]+"></textarea>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Jenis</label>
                        <select class="form-control" name="jenis" style="height: calc(3rem + 2px);">
                            <?php 
                                foreach($db->in_jenis() as $ijs){
                            ?>
                            <option value="<?php echo $ijs['id_jenis'] ?>"><?php echo $ijs['nama_jenis'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Ruangan</label>
                        <select class="form-control" name="ruang" style="height: calc(3rem + 2px);">
                            <?php 
                                foreach($db->in_ruang() as $ir){
                            ?>
                            <option value="<?php echo $ir['id_ruang'] ?>"><?php echo $ir['nama_ruang'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Sarana</label>
                        <select class="form-control" name="sarana" style="height: calc(3rem + 2px);">
                            <?php 
                                foreach($db->sarana() as $j){
                            ?>
                            <option value="<?php echo $j['id_sarana'] ?>"><?php echo $j['nama_sarana'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Kode Inventaris</label>
                        <input class="form-control" type="text" name="kode_inventaris" maxlength="10" id="example-text-input" required="" pattern="[a-z A-Z 0-9]+" autocomplete="off">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Upload</span>
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="inputGroupFile01" name="foto" accept="image/*" required>
                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4"> Simpan</button>
                    </form>
                </div>
            </div>
        </div>
</div>