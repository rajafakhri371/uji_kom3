<div class="card-area">
        <div class="row">
            <?php
            foreach($db->detail_lihat($_GET['id_sarana']) as $d){
            ?>
            <div class="col-lg-3 col-md-3 mt-3">
                <div class="card card-bordered">
                    <img class="card-img-top img-fluid" src="assets/images/inventaris/<?php echo $d['foto'] ?>" style="height: 150px;" alt="image">
                    <div class="card-body">
                        <h5 class="title"><?php echo $d['nama'] ?></h5>
                        <p class="card-text">
                        </p>
                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg-<?=$d['id_inventaris'];?>">Go More....</a>
                    </div>
                </div>      
            </div>
             <div class="modal fade bd-example-modal-lg-<?=$d['id_inventaris'];?>">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"><?php echo $d['nama'] ?></h5>
                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <p align="center"><img src="assets/images/inventaris/<?php echo $d['foto'] ?>" style="width: 150px;"></p>
                            <p>
                    <div class="col-lg-12 mt-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="single-table">
                                    <div class="table-responsive">
                                        <table class="table table-striped text-center">
                                            <thead class="text-uppercase">
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">Nama Inventaris</th>
                                                    <td>:</td>
                                                    <td><?php echo $d['nama']?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Kondisi</th>
                                                    <td>:</td>
                                                     <td><?php echo $d['kondisi']?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Keterangan</th>
                                                    <td>:</td>
                                                    <td><textarea class="form-control" style="height: 120px" readonly><?php echo $d['keterangan']?></textarea></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Jumlah</th>
                                                    <td>:</td>
                                                    <td><?php echo $d['jumlah']?></td>                                                    
                                                </tr>
                                                <tr>
                                                    <th scope="row">Kode Inventaris</th>
                                                    <td>:</td>
                                                    <td><?php echo $d['kode_inventaris']?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Jenis</th>
                                                    <td>:</td>
                                                     <td><?php echo $d['nama_jenis']?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Ruang</th>
                                                    <td>:</td>
                                                    <td><?php echo $d['nama_ruang']?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Sarana</th>
                                                    <td>:</td>
                                                    <td><?php echo $d['nama_sarana']?></td>                                                    
                                                </tr>
                                                <tr>
                                                    <th scope="row">Tanggal Penambahan
                                                    </th>
                                                    <td>:</td>
                                                    <td><?php echo date('d F Y', strtotime($d['tanggal_register']));?></td>                                                    
                                                </tr>
                                                <tr>
                                                    <th scope="row">Penambah</th>
                                                    <td>:</td>
                                                    <td><?php echo $d['nama_petugas']?></td>                                                    
                                                </tr>



                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <a href="function/proses.php?id_inventaris=<?php echo $d['id_inventaris']; ?>&aksi=hapus_inventaris&id_sarana=<?php echo $d['id_sarana'] ?>"><button type="button" class="btn btn-danger">Delete</button></a>
                            <a href="?page=inventaris&opsi=edit_inventaris&id_inventaris=<?php echo $d['id_inventaris']; ?>&id_sarana=<?php echo $d['id_sarana'] ?>"><button type="button" class="btn btn-primary">Ubah</button></a>
                        </div>
                    </div>
                </div>
        </div>
            <?php
            }
            ?>


</div>
</div>