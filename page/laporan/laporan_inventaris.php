<?php 

	$koneksi= new mysqli("localhost","root","","uji_kom");

 ?>

 <style>
 	@media print{
 		input.noPrint{
 			display: none;
 		}
 	}
 </style>
<p align="center" id="title-laporan"><img src="bogor.png" width="80px" style="float: left; margin-left: 20px;"><img src="ciomas.png" width="90px" style="float: right;"><b>PEMERINTAH KABUPATEN BOGOR<br>DINAS PENDIDIKAN<br>SMK NEGERI 1 CIOMAS<br>BIDANG STUDI KEAHLIAN TEKNOLOGI DAN REKAYASA<BR>TEKNOLOGI INFORMASI DAN KOMUNIKASI</b></p>

<p align="center" id="title-laporan2">Jln. Raya Laladon Desa Laladon Kec Ciomas Kab Bogor Telp : (0251 7520933) Kode Pos. 16610</p><br>
<table border="1" width="100%" style="border-collapse: collapse;"><hr>
	<caption><h1>Laporan Inventaris</h1></caption>
	<thead>
	<tr>
		<th>NO</th>
		<th>Nama Inventaris</th>
		<th>Kondisi</th>
		<th>Keterangan</th>
		<th>Jumlah Tersedia</th>
		<th>Jumlah Rusak Barang</th>
		<th>Jenis</th>
		<th>Tanggal Register</th>
		<th>Ruang</th>
		<th>Kode Inventaris</th>
		<th>Sarana</th>
		<th>Petugas</th>

	</tr>
	</thead>
	<tbody>
<?php
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
       $no=1;
		$sql=$koneksi->query("SELECT * FROM inventaris i LEFT JOIN jenis j ON i.id_jenis=j.id_jenis LEFT JOIN ruang r ON i.id_ruang=r.id_ruang LEFT JOIN sarana s ON i.id_sarana=s.id_sarana LEFT JOIN detail_pinjam d ON d.id_inventaris_d=i.id_inventaris LEFT JOIN petugas p ON i.id_petugas=p.id_petugas GROUP BY i.id_inventaris");
		while($data=$sql->fetch_array()){
	 ?>
    <tr>
        <td><?php echo $no++; ?></td>
        <td><?php echo $data['nama']; ?></td>
        <td><?php echo $data['kondisi']; ?></td>
        <td><?php echo $data['keterangan']; ?></td>
        <td><?php echo $data['jumlah']; ?></td>
        <td><?php 
       $id_inventaris= $data['id_inventaris'];
                $sum=$koneksi->query("SELECT SUM(rusak_d) as total FROM detail_pinjam WHERE id_inventaris_d='$id_inventaris'");
                    while($da=$sum->fetch_array()){
                    echo $da['total'];
                    }
            ?></td>
        <td><?php echo $data['nama_jenis']; ?></td>
        <td><?php echo date('d F Y', strtotime($data['tanggal_register'])); ?></td>
        <td><?php echo $data['nama_ruang']; ?></td>
        <td><?php echo $data['kode_inventaris']; ?></td>
        <td><?php echo $data['nama_sarana']; ?></td>
        <td><?php echo $data['nama_petugas']; ?></td>


    </tr>
<?php  } ?>
	</tbody>
</table>

<br>
 
 <form method="POST" action="laporan_inventaris_excel.php">
  <input type="button" value="Cetak" class="noPrint" onclick="window.print()">
  <input type="submit" name="export" class="btn btn-success noPrint" value="Export" />
</form> 
