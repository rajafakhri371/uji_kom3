<div class="row">
<div class="col-lg-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Data Sarana</h4>
                    <div class="single-table">
                        <div class="table-responsive">
                            <a href="#" data-toggle="modal" data-target="#tambah_sarana"><button type="button" class="btn btn-primary"><i class="fa fa-plus-square" style="font-size:120%;"></i></button></a>
                            <a href="page/laporan/laporan_sarana.php" target="blank"><button type="button" class="btn btn-primary">Data Sarana</button></a><br>
                            <br>
                            <br></a></span>
                            <table class="table text-center" id="example">
                                <thead class="text-uppercase bg-primary">
                                    <tr class="text-white">
                                        <th scope="col">NO</th>
                                        <th scope="col">Nama Sarana</th>
                                        <th scope="col">Kode Sarana</th>
                                        <th scope="col">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
                                $no = 1;
                                foreach($db->tampil_data_sarana() as $sa){
                                ?>
                                <tr>
                                    <th scope="row"><?php echo $no++; ?></th>
                                    <td><?php echo $sa['nama_sarana']; ?></td>
                                    <td><?php echo $sa['kode_sarana']; ?></td>
                                    <td>
                                        <a href="#" data-toggle="modal" data-target="#edit_sarana-<?php echo $sa['id_sarana']; ?>"><i class="ti-pencil-alt"></i></a> ||
                                        <a href="function/proses.php?id_sarana=<?php echo $sa['id_sarana']; ?>&aksi=hapus_sarana"><i class="ti-trash"></i></a>          
                                    </td>
                                </tr>

                                <!-- Modal -->
                                <div class="modal fade" id="edit_sarana-<?php echo $sa['id_sarana']; ?>">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Edit Data Sarana</h5>
                                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <form action="function/proses.php?aksi=update_sarana" method="POST" enctype="multipart/form-data">
                                                            <div class="form-group">
                                                                <input class="form-control" type="hidden" name="id_sarana" id="example-text-input" required="" value="<?php echo $sa['id_sarana']; ?>">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="example-text-input" class="col-form-label">Nama Sarana</label>
                                                                <input class="form-control" type="text" name="nama_sarana" maxlength="25" id="example-text-input" required="" value="<?php echo $sa['nama_sarana']; ?>" pattern="[a-z A-Z]+" autocomplete="off">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="example-text-input" class="col-form-label">Kode Sarana</label>
                                                                <input class="form-control" type="text" name="kode_sarana" maxlength="5" id="example-text-input" required="" value="<?php echo $sa['kode_sarana']; ?>"pattern="[a-z A-Z 0-9]+" autocomplete="off">
                                                            </div>
                                                            <div class="input-group mb-3">
                                                              <div class="input-group-prepend">
                                                                <span class="input-group-text">Upload</span>
                                                              </div>
                                                            <div class="custom-file">
                                                              <input type="file" class="custom-file-input" id="inputGroupFile01" name="foto" accept="image/*" required="">
                                                                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                                            </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                                                                <button type="submit" class="btn btn-primary">Simpan</button>
                                                            </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        </div>
                                <?php 
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

                <!-- Modal -->
                <div class="modal fade" id="tambah_sarana">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Tambah Data Sarana</h5>
                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                            </div>
                            <div class="modal-body">
                        <div class="row">

                        <div class="col-lg-6 col-ml-12">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <form action="function/proses.php?aksi=tambah_sarana" method="POST" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Nama Sarana</label>
                                            <input class="form-control" type="text" name="nama_sarana" maxlength="25" id="example-text-input" required="" pattern="[a-z A-Z]+">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Kode Sarana</label>
                                            <input class="form-control" type="text" name="kode_sarana" maxlength="5" id="example-text-input" required="" pattern="[a-z A-Z 0-9]+">
                                        </div>
                                        <div class="input-group mb-3">
                                          <div class="input-group-prepend">
                                                <span class="input-group-text">Upload</span>
                                                    </div>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="inputGroupFile01" name="foto" accept="image/*">
                                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                        </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                    </div>
                        </div>
                        </div>
                    </div>
                </div>
        </div>
        </div>
</div>