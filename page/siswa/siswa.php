<div class="row">
<div class="col-lg-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Data siswa</h4>
                    <div class="single-table">
                        <div class="table-responsive">
                            <a href="#" data-toggle="modal" data-target="#tambah_siswa"><button type="button" class="btn btn-primary"><i class="fa fa-plus-square" style="font-size:120%;"></i></button></a>
                            <a href="page/laporan/laporan_data_siswa.php" target="blank"><button type="button" class="btn btn-primary">Data Siswa</button></a><br>
                            <br>
                            <br></a></span>
                            <table class="table text-center" id="example">
                                <thead class="text-uppercase bg-primary">
                                    <tr class="text-white">
                                        <th scope="col">NO</th>
                                        <th scope="col">Nama Siswa</th>
                                        <th scope="col">NIS</th>
                                        <th scope="col">Jenis Kelamin</th>
                                        <th scope="col">Alamat</th>                                  
                                        <th scope="col">No. Hp</th>                                  
                                        <th scope="col">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
                                $no = 1;
                                foreach($db->tampil_data_siswa() as $js){
                                ?>
                                <tr>
                                    <th scope="row"><?php echo $no++; ?></th>
                                    <td><?php echo $js['nama_siswa']; ?></td>
                                    <td><?php echo $js['nis']; ?></td>
                                    <td><?php echo $js['jk']; ?></td>
                                    <td><?php echo $js['alamat']; ?></td>
                                    <td><?php echo $js['no_hp']; ?></td>
                                    <td>
                                        <a href="#" data-toggle="modal" data-target="#edit_siswa-<?php echo $js['id_siswa']; ?>"><i class="ti-pencil-alt"></i></a> ||
                                        <a href="function/proses.php?id_siswa=<?php echo $js['id_siswa']; ?>&aksi=hapus_siswa"><i class="ti-trash"></i></a>          
                                    </td>
                                </tr>

                                <!-- Modal -->
                                <div class="modal fade" id="edit_siswa-<?php echo $js['id_siswa']; ?>">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Edit Data Siswa</h5>
                                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <form action="function/proses.php?aksi=update_siswa" method="POST">
                                                                <div class="form-group">
                                                                    <input class="form-control" type="hidden" name="id_siswa" id="example-text-input" required="" value="<?php echo $js['id_siswa']; ?>">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="example-text-input" class="col-form-label">Nama Siswa</label>
                                                                    <input class="form-control" type="text" name="nama_siswa" maxlength="25" id="example-text-input" required="" value="<?php echo $js['nama_siswa'] ?>" pattern="[a-z A-Z]+" autocomplete="off">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="example-text-input" class="col-form-label">NIS</label>
                                                                    <input class="form-control" type="text" name="nis" maxlength="15" pattern="[0-9]+" id="example-number-input" required="" value="<?php echo $js['nis']?>" pattern="[0-9]+" autocomplete="off">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="example-text-input" class="col-form-label">Jenis Kelamin</label>
                                                                     <select class="form-control" name="jk">
                                                                        <?php
                                                                        if ($js['jk']=="Laki - Laki") {
                                                                            echo "<option value='Laki - Laki' selected>Laki - Laki</option>";
                                                                        }elseif ($js['jk']=="Perempuan") {
                                                                            echo "<option value='Perempuan' selected>Perempuan</option>";
                                                                        }

                                                                        ?>
                                                                         
                                                                </select>
                                                                <div class="form-group">
                                                                    <label for="example-text-input" class="col-form-label">Alamat</label>
                                                                     <textarea name="alamat" class="form-control" id="" cols="30" rows="10" pattern="[a-z A-Z 0-9]+"><?php echo $js['alamat'] ?></textarea>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="example-text-input" class="col-form-label">No. Hp</label>
                                                                    <input class="form-control" type="text" name="no_hp" maxlength="15" pattern="[0-9]+" id="example-number-input" required="" value="<?php echo $js['no_hp']?>" pattern="[0-9]+" autocomplete="off">
                                                                </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                                                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                                                </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                    </div>
                                </div>
                        </div>
                        </div>

                                <?php 
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                <!-- Modal -->
                <div class="modal fade" id="tambah_siswa">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Tambah Data Siswa</h5>
                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                            </div>
                            <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <form action="function/proses.php?aksi=tambah_siswa" method="POST">
                                    <div class="form-group">
                                        <label for="example-text-input" class="col-form-label">Nama Siswa</label>
                                        <input class="form-control" type="text" name="nama_siswa" maxlength="25" id="example-text-input" required="" pattern="[a-z A-Z]+">
                                    </div>
                                    <div class="form-group">
                                        <label for="example-text-input" class="col-form-label">NIS</label>
                                        <input class="form-control" type="text" name="nis" maxlength="15" pattern="[0-9]+" id="example-number-input" required="" pattern="[0-9]+">
                                    </div>
                                    <div class="form-group">
                                        <label for="example-text-input" class="col-form-label">Jenis Kelamin</label>
                                        <select class="form-control" name="jk">
                                            <option value="Laki - Laki">Laki - Laki</option>
                                            <option value="Perempuan">Perempuan</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-text-input" class="col-form-label">Alamat</label>
                                         <textarea name="alamat" class="form-control" id="" cols="30" rows="10"></textarea>
                                         <input class="form-control" type="hidden" name="level" id="example-text-input" required="" value="3">
                                    </div>
                                    <div class="form-group">
                                        <label for="example-text-input" class="col-form-label">No.HP</label>
                                        <input class="form-control" type="text" name="no_hp" maxlength="15" pattern="[0-9]+" id="example-number-input" required="" pattern="[0-9]+">
                                    </div>

                                    <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4"> Simpan</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                </div>
                    </div>
                </div>
        </div>
        </div>
</div>