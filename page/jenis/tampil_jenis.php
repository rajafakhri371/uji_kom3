<div class="row">
<div class="col-lg-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Data Jenis</h4>
                    <div class="single-table">
                        <div class="table-responsive">
                            <a href="#" data-toggle="modal" data-target="#tambah_jenis"><button type="button" class="btn btn-primary"><i class="fa fa-plus-square" style="font-size:120%;"></i></button></a>
                            <a href="page/laporan/laporan_data_jenis.php" target="blank"><button type="button" class="btn btn-primary">Data Jenis</button></a><br>
                            <br>
                            <br></a></span>
                            <table class="table text-center" id="example">
                                <thead class="text-uppercase bg-primary">
                                    <tr class="text-white">
                                        <th scope="col">NO</th>
                                        <th scope="col">Nama Jenis</th>
                                        <th scope="col">Kode Jenis</th>
                                        <th scope="col">Keterangan</th>
                                        <th scope="col">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
                                $no = 1;
                                foreach($db->tampil_data_jenis() as $js){
                                ?>
                                <tr>
                                    <th scope="row"><?php echo $no++; ?></th>
                                    <td><?php echo $js['nama_jenis']; ?></td>
                                    <td><?php echo $js['kode_jenis']; ?></td>
                                    <td><?php echo $js['keterangan_j']; ?></td>
                                    <td>
                                        <a href="#" data-toggle="modal" data-target="#edit_jenis-<?php echo $js['id_jenis']; ?>"><i class="ti-pencil-alt"></i></a> ||
                                        <a href="function/proses.php?id_jenis=<?php echo $js['id_jenis']; ?>&aksi=hapus_jenis"><i class="ti-trash"></i></a>          
                                    </td>
                                </tr>

                                <!-- Modal -->
                                <div class="modal fade" id="edit_jenis-<?php echo $js['id_jenis']; ?>">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Edit Data Jenis</h5>
                                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <form action="function/proses.php?aksi=update_jenis" method="POST">
                                                            <div class="form-group">
                                                                <input class="form-control" type="hidden" name="id_jenis" id="example-text-input" required="" value="<?php echo $js['id_jenis']; ?>">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="example-text-input" class="col-form-label">Nama Jenis</label>
                                                                <input class="form-control" type="text" name="nama_jenis" maxlength="25" id="example-text-input" required="" value="<?php echo $js['nama_jenis']; ?>" pattern="[a-z A-Z]+" autocomplete="off">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="example-text-input" class="col-form-label">Kode Jenis</label>
                                                                <input class="form-control" type="text" name="kode_jenis" maxlength="5" id="example-text-input" required="" value="<?php echo $js['kode_jenis']; ?>"pattern="[a-z A-Z 0-9]+" autocomplete="off">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="example-text-input" class="col-form-label">Keterangan</label>
                                                                <textarea name="keterangan" class="form-control" id="" cols="30" rows="10" pattern="[a-z A-Z 0-9]+"><?php echo $js['keterangan_j']; ?></textarea>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                                                                <button type="submit" class="btn btn-primary">Simpan</button>
                                                            </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        </div>
                                <?php 
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

                <!-- Modal -->
                <div class="modal fade" id="tambah_jenis">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Tambah Data Jenis</h5>
                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                            </div>
                            <div class="modal-body">
                        <div class="row">

                        <div class="col-lg-6 col-ml-12">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <form action="function/proses.php?aksi=tambah_jenis" method="POST">
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Nama Jenis</label>
                                            <input class="form-control" type="text" name="nama_jenis" maxlength="25" id="example-text-input" required="" pattern="[a-z A-Z]+" autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Kode Jenis</label>
                                            <input class="form-control" type="text" name="kode_jenis" maxlength="5" id="example-text-input" required="" pattern="[a-z A-Z 0-9]+" autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Keterangan</label>
                                            <textarea name="keterangan" class="form-control" id="" cols="30" rows="10" pattern="[a-z A-Z 0-9]+"></textarea>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                    </div>
                        </div>
                        </div>
                    </div>
                </div>
        </div>
        </div>
</div>