<div class="row">
<div class="col-lg-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Data Petugas</h4>
                    <div class="single-table">
                        <div class="table-responsive">
                            <a href="#" data-toggle="modal" data-target="#tambah_petugas"><button type="button" class="btn btn-primary"><i class="fa fa-plus-square" style="font-size:120%;"></i></button></a>
                            <a href="page/laporan/laporan_data_petugas.php" target="blank"><button type="button" class="btn btn-primary">Data Petugas</button></a><br>
                            <br>
                            <br></a></span>
                            <table class="table text-center" id="example">
                                <thead class="text-uppercase bg-primary">
                                    <tr class="text-white">
                                        <th scope="col">NO</th>
                                        <th scope="col">Nama Petugas</th>
                                        <th scope="col">Username</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Sebagai</th>
                                        <th scope="col">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
                                $no = 1;
                                foreach($db->tampil_data_petugas() as $dat){
                                ?>
                                <tr>
                                    <th scope="row"><?php echo $no++; ?></th>
                                    <td><?php echo $dat['nama_petugas']; ?></td>
                                    <td><?php echo $dat['username']; ?></td>
                                    <td><?php echo $dat['email']; ?></td>
                                    <td><?php echo $dat['nama_level']; ?></td>
                                    <td>
                                        <a href="#" data-toggle="modal" data-target="#edit_petugas-<?php echo $dat['id_petugas']; ?>"><i class="ti-pencil-alt"></i></a> ||
                                        <a href="function/proses.php?id_petugas=<?php echo $dat['id_petugas']; ?>&aksi=hapus_petugas"><i class="ti-trash"></i></a>          
                                    </td>
                                </tr>
                                
                                <!-- Modal -->
                                <div class="modal fade" id="edit_petugas-<?php echo $dat['id_petugas']; ?>">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Edit Data Petugas</h5>
                                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <form action="function/proses.php?aksi=update_petugas" method="POST">
                                                            <div class="form-group">
                                                                <input class="form-control" type="hidden" name="id_petugas" id="example-text-input" required="" value="<?php echo $dat['id_petugas']; ?>" autocomplete="off">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="example-text-input" class="col-form-label">Nama Petugas</label>
                                                                <input class="form-control" type="text" name="nama_petugas" maxlength="25" id="example-text-input" required="" value="<?php echo $dat['nama_petugas'] ?>" pattern="[a-z A-Z]+" autocomplete="off">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="example-text-input" class="col-form-label">Username</label>
                                                                <input class="form-control" type="text" name="username" maxlength="25" id="example-text-input" required="" value="<?php echo $dat['username'] ?>" pattern="[a-z A-Z 0-9]+" autocomplete="off">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="example-text-input" class="col-form-label">Password</label>
                                                                <input class="form-control" type="Password" name="password" maxlength="25" id="example-text-input" required="" pattern="[a-z A-Z 0-9]+">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="example-text-input" class="col-form-label">Email</label>
                                                                <input class="form-control" type="email" name="email" id="example-text-input" required="" value="<?php echo $dat['email'] ?>" autocomplete="off">
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-form-label">Aktivasi</label>
                                                                <select class="form-control" name="level" style="height: calc(3rem + 2px);">
                                                                    <?php 
                                                                        foreach($db->tampil_data_level() as $js){
                                                                            if ($js['id_level']==$dat['id_level']) {
                                                                                $selected="selected";
                                                                            }else{
                                                                                $selected="";
                                                                            }
                                                                    ?>
                                                                    <option value="<?php echo $js['id_level'] ?>" <?php echo $selected ?> ><?php echo $js['nama_level'] ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                                                                <button type="submit" class="btn btn-primary">Simpan</button>
                                                            </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        </div>

                                <?php 
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                <!-- Modal -->
                <div class="modal fade" id="tambah_petugas">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Tambah Petugas</h5>
                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                            </div>
                            <div class="modal-body">
                        <div class="row">

                        <div class="col-lg-6 col-ml-12">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <form action="function/proses.php?aksi=tambah_petugas" method="POST">
                                            <div class="form-group">
                                                <label for="example-text-input" class="col-form-label">Nama Petugas</label>
                                                <input class="form-control" type="text" name="nama_petugas" maxlength="25" id="example-text-input" required="">
                                            </div>
                                            <div class="form-group">
                                                <label for="example-text-input" class="col-form-label">Username</label>
                                                <input class="form-control" type="text" name="username" maxlength="25" id="example-text-input" required="">
                                            </div>
                                            <div class="form-group">
                                                <label for="example-text-input" class="col-form-label">Password</label>
                                                <input class="form-control" type="Password" name="password" maxlength="25" id="example-text-input" required="">
                                            </div>
                                            <div class="form-group">
                                                <label for="example-text-input" class="col-form-label">Email</label>
                                                <input class="form-control" type="email" name="email" maxlength="25" id="example-text-input" required="">
                                            </div>
                                            <div class="form-group">
                                                <label class="col-form-label">Aktivasi</label>
                                                <select class="form-control" name="level" style="height: calc(3rem + 2px);">
                                                    <?php 
                                                        foreach($db->tampil_data_level() as $js){
                                                    ?>
                                                    <option value="<?php echo $js['id_level'] ?>"><?php echo $js['nama_level'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                        </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
</div>
</div>