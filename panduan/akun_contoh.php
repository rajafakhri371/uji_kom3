<?php
include"header_help.php";
?>
    <div class="col-xl-12 col-lg-8 mt-5">
    <div class="card">
    <div class="card-body">
        <div class="col-lg-12 mt-5">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="header-title">Contoh Akun Pemakaian</h4>
                                <div class="single-table">
                                    <div class="table-responsive">
                                        <table class="table text-center">
                                            <thead class="text-uppercase bg-primary">
                                                <tr class="text-white">
                                                    <th scope="col">No.</th>
                                                    <th scope="col">Pengguna</th>
                                                    <th scope="col">Username</th>
                                                    <th scope="col">Password</th>
                                                    <th scope="col">NIP</th>
                                                    <th scope="col">NIS</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>Administrator</td>
                                                    <td>admin</td>
                                                    <td>admin</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">2</th>
                                                    <td>Operator</td>
                                                    <td>operator</td>
                                                    <td>operator</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">3</th>
                                                    <td>Pegawai</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>870238412341232</td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">4</th>
                                                    <td>Siswa</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>123412512342123</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

    </div>
    </div>
    </div>
<?php
include"footer_help.php";
?>