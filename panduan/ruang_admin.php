<?php
include"header_help.php";
?>
    <div class="col-xl-12 col-lg-8 mt-5">
    <div class="card">
    <div class="card-body">
    <h2>Ruang</h2><br>
    <div class="content">
    	<style>
    	img{
    		max-width: 65%;
    	}
        </style>
    	<h3>Tambah Ruang</h3><br>
    	<table style="font-size: 130%">
    		<tr>
    			<td>1. </td>
    			<td>Masuk/Log in Dengan Hak Akses Admin</td>
    		</tr>
    		<tr>
    			<td>2. </td>
    			<td>Masuk ke halaman Ruang</td>
    		</tr>
    		<tr>
    			<td>3. </td>
    			<td>Pilih Tanda Plus + di atas, seperti kotak merah</td>
    		</tr>
    		<tr>
    			<td></td>
    			<td><img src="foto/ruang.png"></td>
    		</tr>
    		<tr>
    			<td>4. </td>
    			<td>Kemudian isi form untuk tambah Ruang</td>
    		</tr>
    		<tr>
    			<td></td>
    			<td><img src="foto/Ruang_tambah.png"></td>
    		</tr>
    		<tr>
    			<td>5. </td>
    			<td>Setelah selesai klik simpan</td>
    		</tr>
    	</table><br>

    	<h3>Mengubah Ruang</h3><br>
    	<table style="font-size: 130%">
    		<tr>
    			<td>1. </td>
    			<td>Masuk/Log in Dengan Hak Akses Admin</td>
    		</tr>
    		<tr>
    			<td>2. </td>
    			<td>Masuk ke halaman Ruang</td>
    		</tr>
    		<tr>
    			<td>3. </td>
    			<td>Pilih ikon ubah, seperti kotak coklat</td>
    		</tr>
    		<tr>
    			<td></td>
    			<td><img src="foto/Ruang.png"></td>
    		</tr>
    		<tr>
    			<td>4. </td>
    			<td>Kemudian ubah Ruang</td>
    		</tr>
    		<tr>
    			<td>5. </td>
    			<td>Setelah selesai klik simpan</td>
    		</tr>
    	</table><br>

    	<h3>Hapus Ruang</h3><br>
    	<table style="font-size: 130%">
    		<tr>
    			<td>1. </td>
    			<td>Ikuti langkah - langkah sepeti mengubah Ruang dari nomor 1-2</td>
    		</tr>
    		<tr>
    			<td>2. </td>
    			<td>Kemudian pilih Ruang yang akan di Hapus, Lalu klik ikon hapus seperti kotak orange</td>
    		</tr>
    		<tr>
    			<td></td>
    			<td><img src="foto/Ruang.png"></td>
    		</tr>
    	</table>
    </div>
    </div>
    </div>
    </div>
<?php
include"footer_help.php";
?>