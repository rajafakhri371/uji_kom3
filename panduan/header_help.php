
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Panduan Pemakaian</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="../assets/images/icon/favicon.ico">
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="../assets/css/metisMenu.css">
    <link rel="stylesheet" href="../assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="../assets/css/slicknav.min.css">
    <!-- amchart css -->
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <!-- others css -->
    <link rel="stylesheet" href="../assets/css/typography.css">
    <link rel="stylesheet" href="../assets/css/default-css.css">
    <link rel="stylesheet" href="../assets/css/styles.css">
    <link rel="stylesheet" href="../assets/css/responsive.css">
    <!-- modernizr css -->
    <script src="../assets/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body class="body-bg">
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->
    <!-- main wrapper start -->
    <div class="horizontal-main-wrapper">
        <!-- main header area start -->
        <div class="mainheader-area">
            <div class="container">
                <div class="row align-items-center">                                
                </div>
            </div>
        </div>
        <!-- main header area end -->
        <!-- header area start -->
        <div class="header-area header-bottom">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-9  d-none d-lg-block">
                        <div class="horizontal-menu">
                            <nav>
                                <ul id="nav_menu">
                                    <li>
                                        <a href="javascript:void(0)"><i class="ti-user"></i><span>Admin</span></a>
                                        <ul class="submenu">
                                            <li><a href="inventaris_admin.php">Inventaris</a></li>
                                            <li><a href="jenis_admin.php">Jenis</a></li>
                                            <li><a href="ruang_admin.php">Ruang</a></li>
                                            <li><a href="tambah_petugas_admin.php">Tambah Petugas</a></li>
                                            <li><a href="tambah_pegawai.php">Tambah Pegawai</a></li>
                                            <li><a href="peminjaman_admin.php">Peminjaman</a></li>
                                            <li><a href="pengembalian_admin.php">Pengembalian</a></li>                                            
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><i class="ti-user"></i><span>Operator</span></a>
                                        <ul class="submenu">
                                            <li><a href="peminjaman_operator.php">Peminjaman</a></li>
                                            <li><a href="pengembalian_operator.php">Pengembalian</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><i class="ti-user"></i><span>Pegawai</span></a>
                                        <ul class="submenu">
                                            <li><a href="peminjaman_pegawai.php">Peminjaman</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><i class="ti-user"></i><span>Siswa</span></a>
                                        <ul class="submenu">
                                            <li><a href="peminjaman_siswa.php">Peminjaman</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="akun_contoh.php"><i class="ti-user"></i><span>Contoh Username</span></a>
                                    </li>
                                    <li>
                                        <a href="buku_panduan.php"><i class="ti-user"></i><span>Buku Panduan</span></a>
                                    </li>
                                    <li>
                                        <a href="../login.php"><i class="ti-user"></i><span>Kembali</span></a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <!-- mobile_menu -->
                    <div class="col-12 d-block d-lg-none">
                        <div id="mobile_menu"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- header area end -->
        <!-- page title area end -->
        <div class="main-content-inner">