<?php
include"header_help.php";
?>
    <div class="col-xl-12 col-lg-8 mt-5">
    <div class="card">
    <div class="card-body">
    <h2>Inventaris</h2><br>
    <div class="content">
    	<style>
    	img{
    		max-width: 65%;
    	}
        </style>
    	<h3>Tambah Inventaris</h3><br>
    	<table style="font-size: 130%">
    		<tr>
    			<td>1. </td>
    			<td>Masuk/Log in Dengan Hak Akses Admin</td>
    		</tr>
    		<tr>
    			<td>2. </td>
    			<td>Masuk ke halaman Inventaris</td>
    		</tr>
    		<tr>
    			<td>3. </td>
    			<td>Pilih Tanda Plus + di atas</td>
    		</tr>
    		<tr>
    			<td></td>
    			<td><img src="foto/inventaris.png"></td>
    		</tr>
    		<tr>
    			<td>4. </td>
    			<td>Kemudian isi form untuk identitas barang</td>
    		</tr>
    		<tr>
    			<td></td>
    			<td><img src="foto/tambah_inventaris.png"></td>
    		</tr>
    		<tr>
    			<td>5. </td>
    			<td>Setelah selesai klik simpan</td>
    		</tr>
    	</table><br>

    	<h3>Mengubah Inventaris</h3><br>
    	<table style="font-size: 130%">
    		<tr>
    			<td>1. </td>
    			<td>Masuk/Log in Dengan Hak Akses Admin</td>
    		</tr>
    		<tr>
    			<td>2. </td>
    			<td>Masuk ke halaman Inventaris</td>
    		</tr>
    		<tr>
    			<td>3. </td>
    			<td>Go More</td>
    		</tr>
    		<tr>
    			<td></td>
    			<td><img src="foto/inventaris.png"></td>
    		</tr>
    		<tr>
    			<td>4. </td>
    			<td>Kemudian pilih barang yang akan di ubah, Lalu klik ubah</td>
    		</tr>
    		<tr>
    			<td></td>
    			<td><img src="foto/edit.png"></td>
    		</tr>
    		<tr>
    			<td>5. </td>
    			<td>Kemudian Isi Data yang akan di simpan</td>
    		</tr>
    		<tr>
    			<td></td>
    			<td><img src="foto/edit2.png"></td>
    		</tr>
    		<tr>
    			<td>6. </td>
    			<td>Setelah selesai klik simpan</td>
    		</tr>
    	</table><br>

    	<h3>Hapus Inventaris</h3><br>
    	<table style="font-size: 130%">
    		<tr>
    			<td>1. </td>
    			<td>Ikuti langkah - langkah sepeti mengubah inventaris dari nomor 1-3</td>
    		</tr>
    		<tr>
    			<td>2. </td>
    			<td>Kemudian pilih barang yang akan di Hapus, Lalu klik hapus</td>
    		</tr>
    		<tr>
    			<td></td>
    			<td><img src="foto/edit.png"></td>
    		</tr>
    	</table>
    </div>
    </div>
    </div>
    </div>
<?php
include"footer_help.php";
?>